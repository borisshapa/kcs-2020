#!/bin/bash

cd $1

cmake . > /dev/null
make > /dev/null

for i in {1..1000}; do
	test=`./script`

	code1_result=`./code1 <<< $test`
	code2_result=`./code2 <<< $test`

	diff=`diff <(echo "$code1_result") <(echo "$code2_result")`
	if [[ $diff != "" ]]; then
		echo "$diff"
		echo -e "\nTEST:"
		echo "$test"
		exit
	fi
done

echo "Outputs for 1000 tests are the same."
